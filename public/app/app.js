angular.module("myApp", ['mainCtrl', 'authService', 'appRoutes', 'userCtrl', 'userService', 'ngRoute', 'storyService', 'storyCtrl', 'reverseDirective'])

    .config(function ($httpProvider) {
        $httpProvider.interceptors.push('AuthInterceptor');
    });