 var mongoose = require('mongoose');

 var Schema = mongoose.Schema;

 var StorySchema = new Schema({
     // we need the creator ID and its in user schema so we are referencing it
     creator: {type: Schema.Types.ObjectId, ref: 'User'},
     content: String,
     created: {type:Date, default:Date.now}
 });

 module.exports = mongoose.model('Story', StorySchema);