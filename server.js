var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
var mongoose = require('mongoose');


var http = require('http').Server(app);
var io = require('socket.io')(http);


var config = require('./config');

mongoose.connect(config.db ,function (err) {
    if(err)
    {
        console.log(err);
    } else{
        console.log('connected to Mongolab!!');
    }
});


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var api = require('./app/routes/api')(app, express, io);

app.use('/api', api);

// it will render all the css and javascript static files to the index.html
app.use(express.static(__dirname + '/public'));

app.get('*', function (req, res) {
    res.sendFile(__dirname + '/public/app/index.html')
})

http.listen(config.PORT, function (err) {
    if(err){
        console.log(err);
    }
    else{
        console.log('Magic happens on port ' + config.PORT);
    }
});
